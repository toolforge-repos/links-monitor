# Links-Monitor

This is a group to monitor external links in Wikimedia projects.

## Description

Links-Monitor is a tool that monitors external links in Wikimedia projects. It helps you keep track of the links and notifies you if any of them are broken.

## Installation

1. Clone the repository.
2. Install dependencies using composer:
    ```bash
    composer install
    ```
3. Set up your configuration file (`.env`) with the necessary parameters.

## Usage

1. Run the application:
    ```bash
    php src/Main.php
    ```
2. The application will start monitoring the external links and provide notifications if any of them are broken.

## Configuration

You can configure the application by modifying the file `.env` and `data` folder files . The following files are available:

- `Thread.json`: The list of thread languages.
- `WikiAvailable.json`: The list of Wikis Available.
- `Whitelist.json`: The list of allowed domains.

## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvement, please open an issue or submit a pull request.

## License

This project is licensed under the [GPL-3.0-or-later license](https://www.gnu.org/licenses/gpl-3.0.en.html).
This is a group to monitor external links in Wikimedia projects
