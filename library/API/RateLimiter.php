<?php
namespace API;

/**
 * Class RateLimiter
 *
 * This class is used to limit the number of requests within a specific time frame.
 */
class RateLimiter {
    /**
     * @var array $timestamps An array of timestamps of requests.
     */
    private $timestamps = [];

    /**
     * @var int $limit The maximum number of requests allowed within the time frame.
     */
    private $limit;

    /**
     * @var int $timeFrame The time frame in seconds within which the limit applies.
     */
    private $timeFrame;

    /**
     * RateLimiter constructor.
     *
     * @param int $limit The maximum number of requests allowed within the time frame.
     * @param int $timeFrame The time frame in seconds within which the limit applies.
     */
    public function __construct(int $limit, int $timeFrame) {
        $this->limit = $limit;
        $this->timeFrame = $timeFrame;
    }

    /**
     * Checks if the number of requests within the time frame exceeds the limit.
     * If so, the method sleeps for one second to avoid exceeding the rate limit.
     */
    public function check() {
        $currentTime = microtime(true);
        $this->timestamps[] = $currentTime;

        // Remove timestamps that are older than the time frame
        $this->timestamps = array_filter($this->timestamps, function($timestamp) use ($currentTime) {
            return ($currentTime - $timestamp) <= $this->timeFrame;
        });

        if (count($this->timestamps) > $this->limit) {
            usleep(1000000); // Sleep for 1 second
            // Clear the timestamps to avoid repeated sleep.
            $this->timestamps = [];
        }
    }
}

