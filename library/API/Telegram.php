<?php
namespace API;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class Telegram
{
    private Client $client;
    private RateLimiter $rateLimiter;
    /**
     * Telegram constructor.
     * @param string $token
     * @param array $headers
     */
    public function __construct(string $token, array $headers = [], $debug = false)
    {
        $this->rateLimiter = new RateLimiter(30, 1);
        $this->client = new Client([
            "base_uri" => "https://api.telegram.org/bot{$token}/",
            "headers" => $headers,
            "debug" => $debug 
        ]);
    }

    /**
     * Send a message to a Telegram chat.
     * @param int $chatId
     * @param string $message
     * @param array $extraParams
     * @return array
     * @throws \Exception
     */
    public function sendMessage($chatId, string $message, $extraParams = []): array
    {
        // Rate limit the request to Telegram API 30 times in 1 second
        $this->rateLimiter->check();
        $endpoint = "sendMessage";
        $params = [
            "chat_id" => $chatId,
            "text" => $message
        ];

        return $this->request("POST", $endpoint, array_merge($params, $extraParams));
    }

    /**
     * Make a request to the Telegram API.
     * @param string $method
     * @param string $endpoint
     * @param array $params
     * @return array
     * @throws \Exception
     */
    private function request(string $method, string $endpoint, array $params): array
    {
        try {
            $response = $this->client->request($method, $endpoint, [
                "json" => $params
            ]);

            $body = json_decode($response->getBody()->getContents(), true);
            if (isset($body["ok"]) && !$body["ok"]) {
                throw new \Exception("Telegram API error: " . $body["description"]);
            }

            return $body;
        } catch (RequestException $e) {
            throw new \Exception("HTTP request error: " . $e->getMessage());
        }
    }
}
?>
