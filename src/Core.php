<?php
namespace App;

use API\Telegram;
use WikiConnect\MediawikiApi\Client\Auth\NoAuth;
use WikiConnect\MediawikiApi\Client\Action\ActionApi;
use WikiConnect\MediawikiApi\MediawikiFactory;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


/**
 * The Core class provides a set of static methods for accessing various
 * services and resources used by the application.
 */
class Core {

    /**
     * @var array The environment variables.
     */
    private static array $env = [];

    /**
     * @var Telegram|null The Telegram API instance.
     */
    private static ?Telegram $apiTelegram = null;

    /**
     * @var Logger|null The logger instance.
     */
    private static ?Logger $logger = null;

    /**
     * Loads the environment variables from the '.env' file.
     *
     * This function reads the '.env' file located in the application's
     * directory and parses its contents into an associative array. The
     * resulting array is then assigned to the `$env` property.
     *
     * If the '.env' file cannot be read or parsed, an exception is thrown.
     *
     * @throws \Exception If the '.env' file cannot be read or parsed.
     */
    public static function load(): void {
        // Read the '.env' file and parse its contents into an associative array.
        self::$env = parse_ini_file(dirname(__DIR__) . "/.env");

        // If the '.env' file cannot be read or parsed, throw an exception.
        if (self::$env === false) {
            throw new \Exception("Failed to load environment file.");
        }
    }

    /**
     * Returns the logger instance. If it doesn't exist, creates a new one and
     * configures it to write logs to the 'app.log' file in the application's
     * directory. The logger is configured to log messages with DEBUG level.
     *
     * @return Logger The logger instance.
     */
    public static function getLogger(): Logger {
        // If the logger instance doesn't exist, create a new one.
        if (self::$logger == null) {
            // Create a new logger instance.
            self::$logger = new Logger('app');

            // Configure the logger to write logs to the 'app.log' file in the
            // application's directory. The logger is configured to log messages
            // with DEBUG level.
            self::$logger->pushHandler(new StreamHandler(
                dirname(__DIR__) . '/app.log',
                Logger::DEBUG
            ));
        }

        // Return the logger instance.
        return self::$logger;
    }

    /**
     * Get the Telegram API instance.
     *
     * This function ensures that the Telegram API instance is loaded and
     * configured with the token obtained from the environment variables. If the
     * instance doesn't exist, it creates a new one and configures it with the
     * token.
     *
     * @return Telegram The Telegram API instance.
     * @throws \Exception If the Telegram token is not set in the environment variables.
     */
    public static function getApiTelegram(): Telegram {
        // Ensure that the environment variables are loaded.
        self::ensureEnvLoaded();

        // If the Telegram API instance doesn't exist, create a new one and
        // configure it with the token obtained from the environment variables.
        if (self::$apiTelegram === null) {
            // Check if the Telegram token is set in the environment variables.
            if (!isset(self::$env["token_telegram"])) {
                throw new \Exception("Telegram token is not set in environment variables.");
            }

            // Create a new Telegram API instance and configure it with the token.
            self::$apiTelegram = new Telegram(self::$env["token_telegram"]);
        }

        // Return the Telegram API instance.
        return self::$apiTelegram;
    }
    /**
     * Get the thread ID for a given server name.
     *
     * This function takes a server name and returns the corresponding thread ID
     * for that server. It first ensures that the environment variables are
     * loaded, and then extracts the language from the server name using a
     * regular expression. It then reads the thread IDs from the "Thread.json"
     * file and returns the thread ID for the given language. If the language
     * is not found, it throws an exception.
     *
     * @param string $server_name The name of the server.
     * @return string|null The thread ID for the server, or null if not found.
     * @throws \Exception If the server name format is invalid.
     */
    public static function getThreadId(string $server_name): ?string {
        // Ensure that the environment variables are loaded.
        self::ensureEnvLoaded();

        // Extract the language from the server name using a regular expression.
        if (preg_match("/^(.*)\..*\..*/", $server_name, $matches)) {
            $language = $matches[1];

            // Read the thread IDs from the "Thread.json" file.
            $thread = json_decode(file_get_contents(dirname(__DIR__) . "/data/Thread.json"), true);

            // Return the thread ID for the given language.
            return $thread[$language] ?? null;
        } else {
            // If the server name format is invalid, throw an exception.
            throw new \Exception("Invalid server name format.");
        }
    }

    /**
     * Checks if the given wiki is available.
     *
     * This function takes a wiki name and checks if it is available in the
     * "WikiAvailable.json" file. It reads the list of available wikis from the
     * file and checks if the given wiki is in the list. If the wiki is found,
     * it returns true; otherwise, it returns false.
     *
     * @param string $wiki The name of the wiki.
     * @return bool Returns true if the wiki is available, false otherwise.
     */
    public static function CheckWiki(string $wiki): bool {
        // Read the list of available wikis from the file.
        $Wikis = json_decode(file_get_contents(dirname(__DIR__) . "/data/WikiAvailable.json"), true);

        // Check if the given wiki is in the list.
        return in_array($wiki, $Wikis);
    }
    /**
     * Retrieves the whitelist of allowed domains.
     *
     * This function reads the whitelist from the "Whitelist.json" file and
     * returns it as an array. The whitelist contains a list of domains that
     * are allowed to be processed by the application.
     *
     * @return array The whitelist of allowed domains.
     */
    public static function getWhitelist(): array {
        // Read the whitelist from the file.
        // The file contains a JSON array of allowed domains.
        return json_decode(file_get_contents(dirname(__DIR__) . "/data/Whitelist.json"), true);
    }
    /**
     * Returns a MediawikiFactory instance for the specified server name.
     *
     * This function creates a new instance of the MediawikiFactory class,
     * configured with an ActionApi instance for the specified server name.
     * The ActionApi instance is configured with an instance of the NoAuth
     * class, which represents an anonymous user.
     *
     * @param string $server_name The name of the server.
     * @return MediawikiFactory The MediawikiFactory instance.
     */
    public static function getMediawikiFactory(string $server_name): MediawikiFactory {
        // Create a new instance of the NoAuth class.
        $auth = new NoAuth();
        
        // Create a new instance of the ActionApi class, configured with
        // the specified server name and the NoAuth instance.
        $api = new ActionApi("https://${server_name}/w/api.php", $auth);
        
        // Create a new instance of the MediawikiFactory class, configured
        // with the ActionApi instance.
        return new MediawikiFactory($api);
    }

    /**
     * Returns the Telegram chat ID from the environment variables.
     *
     * This function reads the Telegram chat ID from the environment variables
     * and returns it. If the chat ID is not set, an exception is thrown.
     *
     * @return string The Telegram chat ID.
     * @throws \Exception If the chat ID is not set in the environment variables.
     */
    public static function getChatId(): string {
        // Ensure that the environment variables are loaded.
        self::ensureEnvLoaded();

        // Check if the chat ID is set in the environment variables.
        if (!isset(self::$env["chatid"])) {
            // Throw an exception if the chat ID is not set.
            throw new \Exception("Telegram chatid is not set in environment variables.");
        }

        // Return the chat ID.
        return self::$env["chatid"];
    }

    /**
     * Ensures that the environment variables are loaded.
     *
     * This private function checks if the environment variables are already loaded.
     * If not, it calls the load() function to load them.
     *
     * @return void
     */
    private static function ensureEnvLoaded(): void {
        // Check if the environment variables are already loaded.
        if (empty(self::$env)) {
            // If not, load the environment variables.
            self::load();
        }
    }
}