<?php
namespace App;

use DOMDocument;
use DOMXPath;
use Monolog\Logger;
use WikiConnect\MediawikiApi\Exception\UsageException;
/**
 * Class ExtractLinks
 *
 * This class is responsible for extracting links from a diff of two revisions of a wiki page.
 */
class ExtractLinks {
    

    /**
     * Extracts links from a page diff.
     *
     * @param string $server_name name of the wiki server
     * @param int $from revision number of the older revision
     * @param int $to revision number of the newer revision
     * @return array|false an array of links or false if no links were found
     */
    public static function get(string $server_name, int $from, int $to): array | false {
        $mediawikiFactory = Core::getMediawikiFactory($server_name);
        try {
        $response = $mediawikiFactory->newPageComparative()->compareRevisions($from, $to, [
            "difftype" => "inline",
            "utf8" => 1,
            "uselang" => "en",
            "format" => "json"
        ]);
        
        return self::extractLinksFromDiff($response["compare"]["*"]);
        } catch (UsageException $e) {
            var_dump($e->getMessage());
            return false;
        }
    }

    /**
     * Extracts links from the added content of a diff.
     *
     * @param string $input HTML content of the diff
     * @return array|false an array of links or false if no links were found
     */
    private static function extractLinksFromDiff(string $input): array | false {
        // Load the HTML content into a DOM document
        $dom = new DOMDocument();
        @$dom->loadHTML($input);
        
        // Create a DOMXPath object to query the document
        $xpath = new DOMXPath($dom);
        $added = "";
        $deleted = "";
        
        // Find all 'ins' elements, which indicate added content, and concatenate their text values
        $insNodes = $xpath->query("//ins");
        foreach ($insNodes as $node) {
            $added .= $node->nodeValue;
        }

        // Find all 'del' elements, which indicate deleted content, and concatenate their text values
        $delNodes = $xpath->query("//del");
        foreach ($delNodes as $node) {
            $deleted .= $node->nodeValue;
        }
        
        // Extract links from the added content and filter them based on the whitelist
        $links = self::CheckLinks(self::extractLinks($added, $deleted));
        
        // If no links were found, return false
        if (!empty($links)) {
            return $links;
        }
        
        return false;
    }
    
    /**
     * Extracts links from a text.
     *
     * @param string $added the text containing added content
     * @param string $deleted the text containing deleted content
     *
     * @return array an array of links
     */
    private static function extractLinks(string $added, string $deleted): array {
        // Define a regular expression pattern to match URLs
        // The pattern matches URLs with the following format:
        // https:// or http:// followed by one or more letters, digits, special characters, and symbols
        // The domain name must contain at least one dot
        // The domain name must end with one or more letters, digits, special characters, and symbols
        // The URL can also contain any number of additional characters after the domain name
        $pattern = "/\bhttps?:\/\/[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&\/\/=]*)\b/";
        
        // Count the number of added and deleted links
        $addedLinkCount = preg_match_all($pattern, $added, $addedMatches);
        $deletedLinkCount = preg_match_all($pattern, $deleted, $deletedMatches);
        
        // If the number of added links is less than or equal to the number of deleted links, 
        // it indicates that the links were removed, so return an empty array
        if ($addedLinkCount <= $deletedLinkCount) {
            return [];
        } 
        
        // Return the array of matches
        return $addedMatches[0];
    }
    
    /**
     * Extracts the domain from a URL.
     *
     * @param string $url the URL to extract the domain from
     * @return string the domain
     * @throws \Exception if the URL could not be parsed
     */
    private static function getDomainFromUrl($url) {
        $parsedUrl = parse_url($url);
        if (isset($parsedUrl['host'])) {
            return $parsedUrl['host'];
        } else {
            throw new \Exception("Failed to get domain from ${url}.");
        }
    }
    
    /**
     * Filters an array of links based on a whitelist of domains.
     *
     * @param array $_links an array of links
     * @return array an array of links that are not in the whitelist
     */
    private static function CheckLinks(array $_links): array {
        // Get the whitelist from Core
        $whitelist = Core::getWhitelist();
        // Filter the links based on the whitelist
        $links = array_filter($_links, function($link) use ($whitelist) {
            foreach ($whitelist as $regex) {
                if (preg_match('/' . $regex . '/', $link)) {
                    return false;
                }
            }
            return true;
        });
        
        return $links;
    }
    
}
