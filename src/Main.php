<?php

namespace App;

require_once dirname(__DIR__) . '/vendor/autoload.php';

use SseClient\Client;
use Monolog\Logger;
use API\Telegram;

/**
 * The main class of the application.
 */
class Main {
    /**
     * The logger instance.
     *
     * @var Logger|null
     */
    private static ?Logger $logger = null;
    /**
     * @var Telegram|null The Telegram API instance.
     */
    private static ?Telegram $apiTelegram = null;

    /**
     * Runs the application.
     *
     * @return void
     */
    public static function run(): void {
        // Load the core
        Core::load();
        // Get the logger instance
        self::$logger = Core::getLogger();
        // Get the Telegram API instance
        self::$apiTelegram = Core::getApiTelegram();
        // Log the start of the application
        self::$logger->info("Application has started running...");
        // Initialize the client
        self::initializeClient();
    }

    /**
     * Initializes the client with the streaming API.
     *
     * @return void
     */
    private static function initializeClient(): void {
        try {
            // Create a new client for the streaming API
            $client = new Client("https://stream.wikimedia.org/v2/stream/recentchange");
            
            // Iterate over the events in the stream
            foreach ($client->getEvents() as $event) {
                // Check if the event is a message
                if ($event->getEventType() === "message") {
                    // Get the data from the event
                    $data = json_decode($event->getData(), true);
                    // Check if the data is set
                    if(isset($data)){
                        // Check if the event is valid
                        if (self::isValidEvent($data)) {
                            // Get the links from the revision
                            $links = ExtractLinks::get(
                                $data["server_name"],
                                $data["revision"]["old"],
                                $data["revision"]["new"]
                            );
                            // Check if the links are found
                            if ($links !== false) {
                                // Iterate over the links
                                foreach ($links as $link) {
                                    // Post the link to Telegram
                                    self::postToTelegram($data, $link);
                                }
                            }
                        }
                    }
                }
            }
        } catch (\Throwable $error) {
            // Log the error
            self::$logger->debug("An unexpected error occurred", [$error->__toString()]);
        }
    }
    /**
     * Retrieves the user groups of a given user.
     *
     * @param string $serverName The name of the server.
     * @param string $username The username of the user.
     * @return array The user groups of the user.
     */
    private static function getUesrGroup(string $serverName, string $username): array {
        // Create a new instance of the MediawikiFactory class,
        // configured with an ActionApi instance for the specified server name.
        $mediawikiFactory = Core::getMediawikiFactory($serverName);
        
        // Create a new instance of the UserGetter class, configured
        // with the specified username.
        $userGetter = $mediawikiFactory->newUserGetter()->getFromUsername($username);
        
        // Retrieve the user groups of the user.
        return $userGetter->getGroups();
    }
    /**
     * Checks if the event is valid.
     *
     * @param array $data The event data.
     * @return bool Returns true if the event is valid, false otherwise.
     */
    private static function isValidEvent(array $data): bool {
        return isset($data["type"], $data["bot"], $data["wiki"]) &&
               $data["type"] === "edit" &&
               !in_array("autoconfirmed", self::getUesrGroup($data["server_name"], $data["user"])) &&
               !$data["bot"] &&
               Core::CheckWiki($data["wiki"]);
    }

    /**
     * Posts a link to Telegram.
     *
     * @param array  $data The event data.
     * @param string $link The link to post.
     * @return void
     */
    private static function postToTelegram(array $data, string $link): void {
        // Extract data from the event
        $title = $data["title"];
        $user = $data["user"];
        $notifyUrl = $data["notify_url"];
        $serverName = $data["server_name"];

        // Construct the message
        $message = sprintf(
            'Person adding the link: <a href="https://%1$s/wiki/Special:Contributions/%2$s">%2$s</a>' . PHP_EOL .
            'Page title: <a href="https://%1$s/wiki/%3$s">%3$s</a>' . PHP_EOL .
            'Added link: <a href="%4$s">%4$s</a>',
            $serverName, $user, $title, $link
        );

        // Send the message to Telegram
        $response = self::$apiTelegram->sendMessage(Core::getChatId(), $message, [
            "message_thread_id" => Core::getThreadId($serverName),
            "parse_mode" => "HTML",
            "no_webpage" => true,
            "reply_markup" => json_encode([
                "inline_keyboard" => [
                    [
                        ["text" => "Diff", "url" => $notifyUrl]
                    ]
                ]
            ])
        ]);
    }
}

Main::run();
